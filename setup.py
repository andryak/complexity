from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='complexity',
    version='1.0.0',
    description='A library to perform WCET complexity analysis of Python programs',
    long_description=readme(),
    keywords='complexity worst case python algorithm program analysis',
    url='https://bitbucket.org/andryak/complexity',
    author='Andrea Aquino',
    author_email='andrex.aquino@gmail.com',
    license='GNU GPL v3',
    packages=[
        'complexity',
        'complexity.experimental',
        'complexity.case_studies.carzaniga',
        'complexity.case_studies.others',
        'complexity.case_studies.popular',
    ],
    entry_points = {
        'console_scripts': [
            'wcet=complexity.worst_case:main',
            'genetic-wcet=complexity.experimental.wcet_generator:main',
        ],
    },
    include_package_data=True,
    zip_safe=False
)
