import random
import unittest
import z3

from complexity.symex import Condition
from complexity.symex import Decision
from complexity.symex import DeadEnd
from complexity.symex import Event
from complexity.symex import patch_environ
from complexity.symex import Path
from complexity.symex import RandomWalkSymbolicExecutor
from complexity.symex import SymbolicExecutor


class TestCondition(unittest.TestCase):
    def test_init(self):
        c = Condition('fibonacci', 3)
        self.assertEqual(c.f, 'fibonacci')
        self.assertEqual(c.lineno, 3)

    def test_eq(self):
        c1 = Condition('fibonacci', 3)
        c2 = Condition('fibonacci', 3)
        self.assertEqual(c1, c2)

    def test_not_eq1(self):
        c1 = Condition('fibonacci', 3)
        c2 = Condition('fibonacci', 5)
        self.assertNotEqual(c1, c2)

    def test_not_eq2(self):
        c1 = Condition('fibonacci', 3)
        c2 = Condition('bubblesort', 3)
        self.assertNotEqual(c1, c2)

    def test_repr(self):
        c = Condition('fibonacci', 3)
        self.assertEqual(repr(c), "'fibonacci':3")


class TestDecision(unittest.TestCase):
    def test_init(self):
        c = Condition('fibonacci', 3)
        d = Decision(c, True)
        self.assertEqual(d.condition, c)
        self.assertEqual(d.chosen_branch, True)

    def test_eq(self):
        c1 = Condition('fibonacci', 3)
        c2 = Condition('fibonacci', 3)

        d1 = Decision(c1, True)
        d2 = Decision(c2, True)
        self.assertEqual(d1, d2)

    def test_not_eq1(self):
        c1 = Condition('fibonacci', 3)
        c2 = Condition('fibonacci', 5)

        d1 = Decision(c1, True)
        d2 = Decision(c2, True)
        self.assertNotEqual(d1, d2)

    def test_not_eq2(self):
        c1 = Condition('fibonacci', 3)
        c2 = Condition('fibonacci', 3)

        d1 = Decision(c1, True)
        d2 = Decision(c2, False)
        self.assertNotEqual(d1, d2)

    def test_repr(self):
        c = Condition('fibonacci', 3)
        d = Decision(c, True)
        self.assertEqual(repr(d), "'T'@['fibonacci':3]")


class TestPath(unittest.TestCase):
    def test_init1(self):
        path = Path()
        self.assertEqual(path.decisions, [])

    def test_init2(self):
        d1 = Decision(Condition('fibonacci', 3), True)
        d2 = Decision(Condition('fibonacci', 5), False)
        d3 = Decision(Condition('fibonacci', 7), False)

        path = Path([d1, d2, d3])
        self.assertEqual(path.decisions, [d1, d2, d3])

    def test_append(self):
        d = Decision(Condition('fibonacci', 3), True)
        path = Path()
        path.append(d)
        self.assertEqual(path.decisions, [d])


class TestDeadEnd(unittest.TestCase):
    def test_init(self):
        with self.assertRaises(DeadEnd):
            raise DeadEnd


class TestPatchEnviron(unittest.TestCase):
    def test_patch_environ(self):
        def explore():
            pass

        bool_ = z3.BoolRef.__bool__
        nonzero_ = z3.BoolRef.__nonzero__

        with patch_environ(explore):
            self.assertEqual(z3.BoolRef.__bool__, explore)
            self.assertEqual(z3.BoolRef.__nonzero__, explore)

        self.assertEqual(z3.BoolRef.__bool__, bool_)
        self.assertEqual(z3.BoolRef.__nonzero__, nonzero_)


class TestSymbolicExecutor(unittest.TestCase):
    def test_init(self):
        se = SymbolicExecutor()
        self.assertTrue(isinstance(se.path, Path))
        self.assertIsNone(se.model)
        self.assertIsNone(se.timeout)
        self.assertEqual(len(se.preconditions), 0)
        self.assertEqual(len(se.handlers), 0)

    def test_on_branch(self):
        se = SymbolicExecutor()
        self.assertEqual(se.on_branch(None, None), (True, False))

    def test_reset_persistent_fields(self):
        se = SymbolicExecutor()

        se.timeout = 1
        self.assertEqual(se.timeout, 1)

        se.reset()
        self.assertEqual(se.timeout, 1)

    def test_reset_non_persistent_fields(self):
        se = SymbolicExecutor()

        se.model = {'x': 1}
        self.assertEqual(se.model, {'x': 1})

        se.reset()
        self.assertIsNone(se.model)

    def test_ensure(self):
        se = SymbolicExecutor()
        self.assertEqual(len(se.solver.assertions()), 0)

        se.ensure(z3.Int('x') > 0)
        self.assertEqual(len(se.solver.assertions()), 1)

        se.ensure(z3.Int('x') > 1)
        self.assertEqual(len(se.solver.assertions()), 2)

    def test_add_handler(self):
        def handler(se):
            pass

        se = SymbolicExecutor()
        se.add_handler(Event.ON_RETURN, handler)
        self.assertEqual(len(se.handlers[Event.ON_RETURN]), 1)

    def test_remove_handler(self):
        def handler(se):
            pass

        se = SymbolicExecutor()
        se.add_handler(Event.ON_RETURN, handler)
        se.remove_handler(Event.ON_RETURN, handler)
        self.assertEqual(len(se.handlers[Event.ON_RETURN]), 0)

    def test_run(self):
        from multiprocessing import Manager

        def is_positive(x):
            if x > 0:
                return True
            return False

        manager = Manager()
        outputs = manager.list()

        def on_return(se, output):
            outputs.append(output)

        se = SymbolicExecutor()
        se.add_handler(Event.ON_RETURN, on_return)
        se.run(is_positive, z3.Int('x'))
        self.assertSetEqual(set(outputs), {0, 1})

    def test_run_with_exception(self):
        from multiprocessing import Value

        def must_be_positive(x):
            if x > 0:
                return True
            raise TypeError()

        raised = Value('b', False)

        def on_except(se, e):
            raised.value = True

        se = SymbolicExecutor()
        se.add_handler(Event.ON_EXCEPT, on_except)
        se.run(must_be_positive, z3.Int('x'))
        self.assertTrue(raised.value)

    def test_run_with_timeout(self):
        def loop():
            while True:
                pass

        timeout_triggered = False

        def on_timeout(se):
            nonlocal timeout_triggered
            timeout_triggered = True

        se = SymbolicExecutor()
        se.timeout = 0.5  # seconds.
        se.add_handler(Event.ON_TIMEOUT, on_timeout)
        se.run(loop)
        self.assertTrue(timeout_triggered)

    def test_run_with_precondition(self):
        from multiprocessing import Manager

        def is_positive(x):
            if x > 0:
                return True
            return False

        manager = Manager()
        outputs = manager.list()

        def on_return(se, output):
            outputs.append(output)

        x = z3.Int('x')
        se = SymbolicExecutor()
        se.add_handler(Event.ON_RETURN, on_return)
        se.preconditions.append(x > 5)
        se.run(is_positive, x)

        self.assertSetEqual(set(outputs), {1})
        self.assertEqual(len(se.solver.assertions()), 0)


class TestRandomWalkSymbolicExecutor(unittest.TestCase):
    def test_spawn_processes_are_stocastically_independent(self):
        from multiprocessing import Manager

        def is_positive(x):
            if x > 0:
                return True
            return False

        manager = Manager()
        random_numbers = manager.list()

        def on_after_fork(se, branch):
            for i in range(100):
                random_numbers.append((branch, se.rng.getrandbits(32)))

        x = z3.Int('x')
        se = RandomWalkSymbolicExecutor(random.Random())
        se.add_handler(Event.ON_AFTER_FORK, on_after_fork)
        se.run(is_positive, x)

        random_on_true_branch = [n for branch, n in random_numbers if branch]
        random_on_false_branch = [n for branch, n in random_numbers if not branch]

        self.assertNotEqual(random_on_true_branch, random_on_false_branch)
