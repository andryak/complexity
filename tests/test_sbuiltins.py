import unittest

from complexity.sbuiltins import srange
from complexity.sbuiltins import sabs


class TestSbuiltins(unittest.TestCase):
    def test_srange_length1(self):
        self.assertEqual(10, len(srange(10)))

    def test_srange_length2(self):
        self.assertEqual(8, len(srange(2, 10)))

    def test_srange_length3(self):
        self.assertEqual(4, len(srange(2, 10, 2)))

    def test_srange_length4(self):
        self.assertEqual(8, len(srange(10, 2, -1)))

    def test_srange_length5(self):
        self.assertEqual(4, len(srange(10, 2, -2)))

    def test_srange_length6(self):
        self.assertEqual(0, len(srange(2, 10, -2)))

    def test_srange_length7(self):
        self.assertEqual(0, len(srange(10, 2, 2)))

    def test_srange_iter(self):
        for i in srange(10):
            self.assertLess(i, 10)

    def test_sabs1(self):
        self.assertEqual(sabs(5), 5)

    def test_sabs2(self):
        self.assertEqual(sabs(-5), 5)
