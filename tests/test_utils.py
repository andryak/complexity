import time
import unittest

from complexity.utils import *


class TestUtils(unittest.TestCase):
    def test_nwise(self):
        list_ = list(nwise([1, 2, 3, 4, 5], n=3))
        self.assertListEqual(list_, [(1, 2, 3), (2, 3, 4), (3, 4, 5)])

    def test_pairwise(self):
        list_ = list(pairwise([1, 2, 3, 4]))
        self.assertListEqual(list_, [(1, 2), (2, 3), (3, 4)])

    def test_callerframe(self):
        def outer(depth):
            return callerframe(depth)

        def inner():
            return outer(1), outer(2)

        frame1, frame2 = inner()
        self.assertEqual(frame1.function, 'outer')
        self.assertEqual(frame2.function, 'inner')

    def test_patch_and_unpatch(self):
        class Test(object):
            pass

        obj = Test()
        self.assertFalse(hasattr(obj, 'missing'))

        patch(obj, 'missing', 'no more')
        self.assertTrue(hasattr(obj, 'missing'))
        self.assertEqual(obj.missing, 'no more')

        unpatch(obj, 'missing')
        self.assertFalse(hasattr(obj, 'missing'))

    def test_get_model_of_empty_solver(self):
        solver = z3.Solver()
        self.assertIsNone(get_model(solver))

    def test_get_model_of_non_empty_solver(self):
        solver = z3.Solver()
        solver.check()
        self.assertIsNotNone(get_model(solver))

    def test_true_expr_to_python_value(self):
        expr = z3.BoolVal(True)
        self.assertEqual(to_python_value(expr), True)

    def test_false_expr_to_python_value(self):
        expr = z3.BoolVal(False)
        self.assertEqual(to_python_value(expr), False)

    def test_one_expr_to_python_value(self):
        expr = z3.IntVal(1)
        self.assertEqual(to_python_value(expr), 1)

    def test_conjuncts(self):
        e1 = z3.Int('x1') > 0
        e2 = z3.Int('x2') > 0
        e3 = z3.Int('x3') > 0
        expr = z3.And(z3.And(e1, e2), e3)
        self.assertSetEqual(set(conjuncts(expr)), {e1, e2, e3})

    def test_model_to_dict(self):
        solver = z3.Solver()
        solver.add(z3.Int('x1') == 1)
        solver.add(z3.Int('x2') == 2)
        solver.add(z3.Int('x3') == 3)

        solver.check()
        model = solver.model()
        self.assertEqual(model_to_dict(model), {'x1': 1, 'x2': 2, 'x3': 3})

    def test_clean_benchmark_string(self):
        benchmark_string = '; benchmark\n' \
                           '(set-info :status unknown)\n' \
                           '(declare-fun x () Int)\n' \
                           '(assert\n (> x 0))\n' \
                           '(check-sat)\n'
        expected_string = '(declare-fun x () Int)(assert (> x 0))'
        self.assertEqual(clean_benchmark_string(benchmark_string), expected_string)

    def test_solver_to_pc(self):
        solver = z3.Solver()
        solver.add(z3.Int('x') == 1)
        self.assertEqual(solver_to_pc(solver), '(declare-fun x () Int)(assert (= x 1))')

    def test_to_smt2(self):
        bool_ref = z3.Int('x') > 0
        self.assertEqual(to_smt2(bool_ref), '(declare-fun x () Int)(assert (> x 0))')

    def test_negate(self):
        expr = z3.simplify(z3.Int('x') > 0)
        self.assertEqual(expr, negate(negate(expr)))

    def test_unravel(self):
        def gen():
            while True:
                yield 0
                time.sleep(0.1)

        result = unravel(gen(), timeout=0.25)
        self.assertTrue(all(e == 0 for e in result))

    def test_most_common(self):
        self.assertEqual(most_common([1, 2, 2, 3, 3, 3]), 3)

    def test_override(self):
        class Base:
            def method(self):
                return 1

        class Derived1(Base):
            @override(Base)
            def method(self):
                return 2

        with self.assertRaises(AssertionError):
            class Derived2(Base):
                @override(Base)
                def wrong_method(self):
                    return 2

    def test_true_is_not_contradiction(self):
        self.assertFalse(is_contradiction(z3.BoolVal(True)))

    def test_false_is_contradiction(self):
        self.assertTrue(is_contradiction(z3.BoolVal(False)))

    def test_partial_range_is_not_contradiction(self):
        self.assertFalse(is_contradiction(z3.Int('x') > 0))

    def test_complete_range_is_not_contradiction(self):
        x = z3.Int('x')
        self.assertFalse(is_contradiction(z3.Or(x <= 0, x > 0)))

    def test_empty_range_is_contradiction(self):
        x = z3.Int('x')
        self.assertTrue(is_contradiction(z3.And(x <= 0, x > 0)))

    def test_true_is_tautology(self):
        self.assertTrue(is_tautology(z3.BoolVal(True)))

    def test_false_is_not_tautology(self):
        self.assertFalse(is_tautology(z3.BoolVal(False)))

    def test_partial_range_is_not_tautology(self):
        self.assertFalse(is_tautology(z3.Int('x') > 0))

    def test_complete_range_is_tautology(self):
        x = z3.Int('x')
        self.assertTrue(is_tautology(z3.Or(x <= 0, x > 0)))

    def test_anything_is_implied_by_false(self):
        x = z3.Int('x')
        self.assertTrue(is_implied(x > 1, [z3.BoolVal(False)]))

    def test_large_range_is_implied_by_small_range(self):
        x = z3.Int('x')
        self.assertTrue(is_implied(x > 0, [x > 1]))

    def test_empty_range_is_inconsistent(self):
        x = z3.Int('x')
        self.assertTrue(is_inconsistent([x <= 0, x > 0]))

    def test_non_empty_range_is_not_inconsistent(self):
        x = z3.Int('x')
        self.assertFalse(is_inconsistent([x > 0, x < 10]))

    def test_minimize_repetitions(self):
        e = z3.Int('x') > 0
        self.assertEqual(minimize([e, e, e, e, e]), [e])

    def test_minimize_triangle_inequality(self):
        x, y, z = z3.Ints('x y z')

        e1 = x > y
        e2 = y > z
        e3 = x > z

        self.assertEqual(minimize([e1, e2, e3]), [e1, e2])
        self.assertEqual(minimize([e1, e3, e2]), [e1, e2])
        self.assertEqual(minimize([e3, e1, e2]), [e1, e2])
