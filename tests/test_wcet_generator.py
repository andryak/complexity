import random
import unittest

from complexity.structs import slist
from complexity.experimental.wcet_generator import ga_wcet_generator
from complexity.experimental.wcet_generator import GAConfig


class TestWcetGenerator(unittest.TestCase):
    def test_ga_wcet_generator(self):
        def bubblesort(l):
            for pn in range(len(l) - 1, 0, -1):
                for i in range(pn):
                    if l[i] > l[i+1]:
                        l[i], l[i+1] = l[i+1], l[i]

        config = GAConfig(
            generations=1,
            population_size=4,
            mutation_prob=0.2,
            elite_ratio=0.1,
            timeout=0.1,
            pool_size=None,
            seed=0,
        )

        # Implicity assert it does not crash.
        ga_wcet_generator(
            bubblesort,
            args=(slist('l', 20),),
            kwargs={},
            config=config,
        )
