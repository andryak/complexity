import sys
import unittest

from complexity.tracer import *
from complexity.utils import pick_prob


class TestTracer(unittest.TestCase):
    def test_timeout_tracer(self):
        def f():
            while True:
                pass

        tracer = TimeoutTracer(timeout=0.25)
        with self.assertRaises(StopTracing):
            tracer.trace(f)
        self.assertGreater(tracer.nr_instr, 0)

    def test_limited_tracer(self):
        def f():
            while True:
                pass

        tracer = LimitedTracer(max_instr=10)
        with self.assertRaises(StopTracing):
            tracer.trace(f)
        self.assertEqual(tracer.nr_instr, 10)

    def test_funscope_tracer(self):
        def f():
            # The next line runs one instruction outside of this module.
            pick_prob([1, 2, 3, 4, 5], 2)
            return 1

        tracer1 = Tracer()
        tracer2 = FunScopeTracer()

        tracer1.trace(f)
        tracer2.trace(f)

        self.assertEqual(tracer1.nr_instr, 3)
        self.assertEqual(tracer2.nr_instr, 2)

    def test_traced(self):
        tracer = Tracer()

        @traced(tracer)
        def f():
            r = 0
            for i in range(5):
                r += i
            return r

        output, nr_instr = f()

        self.assertEqual(output, 10)
        self.assertGreaterEqual(nr_instr, 5)

    def test_traced_has_no_side_effects(self):
        tracer = Tracer()

        @traced(tracer)
        def f():
            r = 0
            for i in range(5):
                r += i
            return r

        self.assertIsNone(sys.gettrace())
        f()
        self.assertIsNone(sys.gettrace())

    def test_traced_has_no_side_effects_on_exception(self):
        tracer = Tracer()

        @traced(tracer)
        def f():
            self.assertIsNotNone(sys.gettrace())
            raise NotImplementedError()

        self.assertIsNone(sys.gettrace())
        try:
            f()
        except NotImplementedError:
            pass
        self.assertIsNone(sys.gettrace())

    def test_untraceable(self):
        tracer = Tracer()

        @untraceable
        def g():
            r = 0
            for i in range(100):
                r += 1
            return r

        @traced(tracer)
        def f():
            r = 0
            r += g()
            return r

        output, nr_instr = f()
        self.assertEqual(output, 100)
        self.assertLessEqual(nr_instr, 10)
