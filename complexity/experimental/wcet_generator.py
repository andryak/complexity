import logging
import random
import z3

from collections import namedtuple
from complexity.symex import RandomWalkSymbolicExecutor
from complexity.utils import conjuncts
from complexity.utils import is_inconsistent
from complexity.utils import minimize
from complexity.utils import negate
from complexity.utils import to_smt2
from complexity.worst_case import wcetpp
from functools import lru_cache
from itertools import chain
from math import ceil

from .non_daemonic_pool import NonDaemonicPool


logger = logging.getLogger(__name__)


class Individual:
    def __init__(self, constraint_set, fitness):
        # Automatically minimize constraint sets to simplify single point crossover.
        self.constraint_set = minimize(constraint_set)
        self.fitness = fitness

    @classmethod
    def from_strings(cls, smt2_strings, fitness):
        """Return an individual with the given constraints and fitness.

        Args:
            smt2_strings (:iterable:string): An iterable of strings describing constraints in SMTLIB v2 format.
            fitness (int): The fitness of the individual.

        Returns:
            (Individual): An individual with the given constraints and fitness.
        """
        return Individual([z3.parse_smt2_string(s) for s in smt2_strings], fitness)

    def __eq__(self, other):
        return set(self.constraint_set) == set(other.constraint_set)

    def __hash__(self):
        return hash(tuple(self.constraint_set))

    def __reduce__(self):
        """Make individual picklable by serializing constraints to SMTLIB v2 format.

        Returns:
            (:tuple:(method, :list:string)): The method to deserialize the individual and a list of strings representing
                the constraints of this individual in SMTLIB v2 format.
        """
        constraint_set_as_strings = [to_smt2(constraint) for constraint in self.constraint_set]
        return Individual.from_strings, (constraint_set_as_strings, self.fitness)

    def __repr__(self):
        return f'Individual(fitness={self.fitness}, constraints={self.constraint_set})'


class HallOfFame:
    def __init__(self, n):
        self.n = n
        self.best_individuals = []

    def update(self, population):
        self.best_individuals.extend(population)
        self.best_individuals = list(set(self.best_individuals))
        self.best_individuals.sort(key=lambda individual: -individual.fitness)
        self.best_individuals = self.best_individuals[:self.n]

    def best_individual(self):
        return self.best_individuals[0]

    def __repr__(self):
        return '\n'.join(map(repr, self.best_individuals))


GAConfig = namedtuple('GAConfig', [
    'generations',
    'population_size',
    'mutation_prob',
    'elite_ratio',
    'timeout',
    'pool_size',
    'seed',
])


def pc_to_constraint_set(pc):
    """Transforms a path condition represented as a string to a constraint set."""
    formula = z3.parse_smt2_string(pc)
    formula = z3.simplify(formula)
    return conjuncts(formula)


random_individual = None
"""Global name of the function that generates random individuals, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


crossover = None
"""Global name of the crossover function, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


mutation = None
"""Global name of the mutation function, declared locally by the `ga_wcet_generator` function.

This global reference ensures that the function is picklable.
"""


def ga_wcet_generator(
        f,
        args,
        kwargs,
        config):

    # This makes some inner functions defined as global functions instead.
    # This (rather nasty) trick makes them pickleable.
    global random_individual
    global crossover
    global mutation

    # Initialize the random number generator.
    rng = random.Random()
    rng.seed(config.seed)

    def log_population(population):
        for i, individual in enumerate(sorted(population, key=lambda individual: -individual.fitness)):
            logger.debug(f'{i}) {individual}')

    def log_population_stats(population):
        fitnesses = [individual.fitness for individual in population]
        min_fitness = min(fitnesses)
        max_fitness = max(fitnesses)
        avg_fitness = sum(fitnesses) / len(fitnesses)
        logger.info(f'max: {max_fitness}, min: {min_fitness}, avg: {avg_fitness}')

    def constrained_wcetpp(preconditions):
        se = RandomWalkSymbolicExecutor(rng)
        se.timeout = config.timeout
        se.preconditions = preconditions
        return wcetpp(se, f, args, kwargs)

    def random_individual(random_seed):
        # Set the seed to a random number provided by the main process.
        rng.seed(random_seed)

        profiles = constrained_wcetpp([])
        profile = rng.choice(profiles)
        return Individual(pc_to_constraint_set(profile.pc), profile.cost)

    @lru_cache(4096)
    def evaluate(constraint_set):
        profiles = constrained_wcetpp(constraint_set)
        profile = rng.choice(profiles)
        return Individual(pc_to_constraint_set(profile.pc), profile.cost)

    def roulette_wheel(population):
        fitness_sum = sum(individual.fitness for individual in population)
        pick = rng.uniform(0, fitness_sum)
        current = 0
        for individual in population:
            current += individual.fitness
            if current > pick:
                return individual

    def selection(population, n):
        population = list(population)
        result = []
        for i in range(n):
            individual1 = roulette_wheel(population)
            population.remove(individual1)

            individual2 = roulette_wheel(population)
            population.append(individual1)

            result.append((individual1, individual2))
        return result

    def crossover(random_seed, parents):
        # Set the seed to a random number provided by the main process.
        rng.seed(random_seed)

        individual1, individual2 = parents

        constraints1 = individual1.constraint_set
        constraints2 = individual2.constraint_set

        cp1 = rng.randint(1, len(constraints1) - 1) if len(constraints1) > 1 else 0
        cp2 = rng.randint(1, len(constraints2) - 1) if len(constraints2) > 1 else 0

        def combine(constraints1, constraints2):
            result = list(constraints1)
            for c in constraints2:
                result.append(c)
                if is_inconsistent(result):
                    result.pop()
            return result

        child_constraints1 = combine(constraints1[:cp1], constraints2[cp2:])
        child_constraints2 = combine(constraints2[:cp2], constraints1[cp1:])

        child1 = evaluate(tuple(child_constraints1))
        child2 = evaluate(tuple(child_constraints2))
        return child1, child2

    def delete_mutation(individual, ratio):
        nr_targets = ceil(ratio * len(individual.constraint_set))

        # Remove some random constraints.
        child_constraints = list(individual.constraint_set)
        for _ in range(nr_targets):
            index = rng.randint(0, len(child_constraints) - 1)
            del child_constraints[index]

        child = evaluate(tuple(child_constraints))
        return child

    def negate_mutation(individual, ratio):
        nr_constraints = len(individual.constraint_set)
        nr_targets = ceil(ratio * nr_constraints)

        # Negate some random constraints.
        child_constraints = list(individual.constraint_set)
        for index in rng.sample(range(nr_constraints), nr_targets):
            child_constraints[index] = negate(child_constraints[index])
            # If the negated constraint makes the constraint set inconsistent, revert.
            if is_inconsistent(child_constraints):
                child_constraints[index] = negate(child_constraints[index])

        child = evaluate(tuple(child_constraints))
        return child

    def mutation(random_seed, individual):
        # Set the seed to a random number provided by the main process.
        rng.seed(random_seed)

        if rng.random() < config.mutation_prob:
            if rng.choice([True, False]):
                return delete_mutation(individual, ratio=0.1)
            else:
                return negate_mutation(individual, ratio=0.1)
        return individual

    def elitism(population, n):
        population = sorted(population, key=lambda individual: -individual.fitness)
        return population[:n], population[n:]

    def survival_selection(population, n):
        population = list(population)
        result = []
        for i in range(n):
            individual = roulette_wheel(population)
            population.remove(individual)
            result.append(individual)
        return result

    elite_size = ceil(config.elite_ratio * config.population_size)
    hof = HallOfFame(5)

    # Generate initial population.
    logger.debug('generating initial population')
    random_seeds = [rng.getrandbits(32) for _ in range(config.population_size)]
    with NonDaemonicPool(config.pool_size) as p:
        population = p.map(random_individual, random_seeds)
    hof.update(population)
    log_population(population)

    logger.debug('hall of fame:')
    logger.debug(hof)

    for g in range(config.generations):
        logger.info(f'generation {g}:')

        # Notice: selection generates a list of pairs using roulette wheel. Never generate pairs such as (a, a).
        parents = selection(population, n=(config.population_size // 2))

        random_seeds = [rng.getrandbits(32) for _ in parents]
        with NonDaemonicPool(config.pool_size) as p:
            offspring = list(chain.from_iterable(p.starmap(crossover, zip(random_seeds, parents))))
        hof.update(offspring)

        logger.debug('offspring after crossover:')
        log_population(offspring)

        random_seeds = [rng.getrandbits(32) for _ in offspring]
        with NonDaemonicPool(config.pool_size) as p:
            offspring = p.starmap(mutation, zip(random_seeds, offspring))
        hof.update(offspring)

        logger.debug('offspring after mutation:')
        log_population(offspring)

        elite, population = elitism(population + offspring, n=elite_size)
        population = survival_selection(population, n=(config.population_size - elite_size))
        population.extend(elite)

        logger.debug('generation summary:')
        log_population(population)
        log_population_stats(population)

        logger.debug('hall of fame:')
        logger.debug(hof)

    best_individual = hof.best_individual()
    logger.info('best individual: %s', best_individual)
    return constrained_wcetpp(best_individual.constraint_set)


def estimate_fitness_evaluations(config):
    """Returns the estimated number of fitness evaluations to run `ga_wcet_generator` with the given configuration.

    Returns:
        (int): The estimated number of fitness evaluations to run `ga_wcet_generator` with the given configuration.
    """
    g = config.generations
    p = config.population_size
    mp = config.mutation_prob
    return p*((mp+1)*g + 1)


def estimate_seconds(config):
    """Returns the estimated number of seconds to run `ga_wcet_generator` with the given configuration.

    Returns:
        (float): The estimated number of seconds to run `ga_wcet_generator` with the given configuration.
    """
    return estimate_fitness_evaluations(config) * config.timeout


def estimate_hours(config):
    """Returns the estimated number of hours to run `ga_wcet_generator` with the given configuration.

    Returns:
        (float): The estimated number of hours to run `ga_wcet_generator` with the given configuration.
    """
    return estimate_seconds(config) / 3600


def main():
    import argparse
    import importlib

    from complexity.utils import pp_wcet_profiles

    parser = argparse.ArgumentParser(description='Genetic WCET input generator.')
    parser.add_argument('module', help='path to a python module')
    parser.add_argument('function', help='name of a function within the module')
    parser.add_argument('size', type=int, help='target size of the WCET input to generate')
    parser.add_argument('--args-gen', default='args_gen', help='name of a function to generate positional arguments')
    parser.add_argument('--kwargs-gen', default='kwargs_gen', help='name of a function to generate keyword arguments')
    parser.add_argument('--generations', type=int, default=300, help='number of generations')
    parser.add_argument('--population-size', type=int, default=200, help='population size')
    parser.add_argument('--mutation-prob', type=float, default=0.2, help='mutation probability')
    parser.add_argument('--elite-ratio', type=float, default=0.1, help='elite ratio')
    parser.add_argument('--timeout', type=float, default=1.0, help='timeout for the symbolic execution, in seconds')
    parser.add_argument('--pool-size', type=int, help='number of pool processes to calculate crossover and mutation')
    parser.add_argument('--seed', type=int, default=0, help='seed')
    pargs = parser.parse_args()

    module_ = importlib.import_module(pargs.module)

    args = getattr(module_, pargs.args_gen)(pargs.size)
    kwargs = getattr(module_, pargs.kwargs_gen)(pargs.size)

    # Set the logger to DEBUG level to capture intermediate output.
    formatter = logging.Formatter(fmt='[%(asctime)s] %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    config = GAConfig(
        generations=pargs.generations,
        population_size=pargs.population_size,
        mutation_prob=pargs.mutation_prob,
        elite_ratio=pargs.elite_ratio,
        timeout=pargs.timeout,
        pool_size=pargs.pool_size,
        seed=pargs.seed,
    )

    logger.info(f'configuration: {config}')
    logger.info(f'target size: {pargs.size}')
    logger.info(f'max estimated time: {estimate_hours(config):.2f} hours')

    profiles = ga_wcet_generator(
        getattr(module_, pargs.function),
        args,
        kwargs,
        config,
    )
    pp_wcet_profiles(profiles)


if __name__ == '__main__':
    main()
