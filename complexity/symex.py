import builtins
import logging
import multiprocessing
import os
import signal
import traceback
import z3

from collections import defaultdict
from complexity.sbuiltins import srange
from complexity.sbuiltins import sabs
from complexity.tracer import untraceable
from complexity.utils import callerframe
from complexity.utils import mk_and
from complexity.utils import mk_not
from complexity.utils import override
from complexity.utils import patch
from complexity.utils import solver_assert
from complexity.utils import unpatch
from contextlib import contextmanager
from enum import auto
from enum import Enum


PRINT_TRACEBACK = False
"""If `True`, `SymbolicExecutor.run` prints captured exceptions tracebacks."""

logger = logging.getLogger(__name__)
"""The logger for this module."""


class Condition:
    """A condition in a program."""
    def __init__(self, f, lineno):
        """Create a new condition.

        Args:
            f (str): The name of a function.
            lineno (int): A line number in the function `f`.
        """
        self.f = f
        self.lineno = lineno

    def __hash__(self):
        """Return the hash value of this condition.

        Returns:
            (int): The hash value of this condition.
        """
        return hash((self.f, self.lineno))

    def __eq__(self, other):
        """Compare this condition with an object for equality.

        Args:
            other (object): The object to compare to this condition.

        Returns:
            (bool): `True` if `object` is also a condition referencing the same function and line number as this one.
        """
        if not isinstance(other, Condition):
            return False
        return self.f == other.f and self.lineno == other.lineno

    def __repr__(self):
        """Return a string representing this condition.

        Returns:
            (str): A string representing this condition.
        """
        return f'{self.f!r}:{self.lineno!r}'


class Decision:
    """A decision taken at a condition."""
    def __init__(self, condition, chosen_branch):
        """Create a new decision.

        Args:
            condition (Condition): The condition.
            chosen_branch (bool): The branch chosen at the given condition.
        """
        self.condition = condition
        self.chosen_branch = chosen_branch

    def __hash__(self):
        """Return the hash value of this decision.

        Returns:
            (int): The hash value of this decision.
        """
        return hash((self.condition, self.chosen_branch))

    def __eq__(self, other):
        """Compare this decision with an object for equality.

        Args:
            other (object): The object to compare to this condition.

        Returns:
            (bool): `True` if `object` is also a decision referencing the same condition and branch choice as this one.
        """
        if not isinstance(other, Decision):
            return False
        return self.condition == other.condition and self.chosen_branch == other.chosen_branch

    def __repr__(self):
        """Return a string representing this decision.

        Returns:
            (str): A string representing this decision.
        """
        choice = 'FT'[self.chosen_branch]
        return f'{choice!r}@[{self.condition!r}]'


class Path:
    """A list of decisions describing the branches taken at conditional statements."""

    def __init__(self, decisions=None):
        """Create a new path.

        Args:
            decisions (:list:Decision, optional): The list of decisions to initialize the path.
        """
        self.decisions = decisions or []

    def append(self, decision):
        """Append a decision to this path.

        Args:
            decision (Decision): The decision.
        """
        self.decisions.append(decision)

    def __repr__(self):
        """Return a string representing this path.

        Returns:
            (str): A string representing this path.
        """
        return '.'.join(map(repr, self.decisions))


class DeadEnd(Exception):
    """Exception raised to notify that a path has reached a dead end.

    Note:
        A path has reached a dead end if:

        1. its corresponding path condition is unsatisfiable, or
        2. the `on_branch` function of `SymbolicExecutor` returns an empty tuple.
    """
    pass


@contextmanager
def patch_environ(explore):
    """Monkey patch methods and functions to create a symbolic execution context.

    Args:
        explore (function): The function called on the evaluation of symbolic conditions.
    """
    patch(z3.BoolRef, '__bool__', explore)
    patch(z3.BoolRef, '__nonzero__', explore)
    patch(builtins, 'abs', sabs)
    patch(builtins, 'range', srange)

    try:
        yield
    finally:
        unpatch(z3.BoolRef, '__bool__')
        unpatch(z3.BoolRef, '__nonzero__')
        unpatch(builtins, 'abs')
        unpatch(builtins, 'range')


class Event(Enum):
    """A symbolic execution event.

    Note:
        This enum is used to register handlers for a `SymbolicExecutor`, which are called when specific events trigger.
    """
    ON_BEFORE_BRANCH = auto()
    ON_AFTER_BRANCH = auto()
    ON_BEFORE_FORK = auto()
    ON_AFTER_FORK = auto()
    ON_BEFORE_CHECK = auto()
    ON_AFTER_CHECK = auto()
    ON_RETURN = auto()
    ON_EXCEPT = auto()
    ON_DEAD_END = auto()
    ON_EXIT = auto()
    ON_TIMEOUT = auto()


class SymbolicExecutor:
    """
    A symbolic executor.

    Note:
        It tracks:

            1. the current execution path,
            2. the current path condition, and
            3. the model of the last satisfiable path condition.

        It can be configured with:

            1. a timeout for the symbolic execution of target callables,
            2. a list of preconditions, and
            3. a set of handlers called when specific events trigger.
    """

    def __init__(self):
        """Create a new symbolic executor."""

        self.path = Path()
        self.solver = z3.Solver()
        self.model = None

        self.timeout = None
        self.preconditions = []
        self.handlers = defaultdict(list)

    def on_branch(self, condition, bool_ref):
        """Return a tuple describing the branches of the current condition to explore.

        Note:
            This function is called by `SymbolicExecutor._explore`.

            The possible return values are:

                1. (): no branch will be explored,
                2. (True): only the `True` branch will be explored,
                3. (False): only the `False` branch will be explored,
                4. (True, False): both the `True` and `False` branches will be explored, in this order,
                5. (False, True): both the `False` and `True` branches will be explored, in this order.

        Args:
            condition (Condition): The current condition.
            bool_ref (z3.BoolRef): The boolean expression corresponding to the current condition.

        Returns:
            (tuple): A tuple of at most two boolean values describing the branches to explore.
        """
        return True, False

    def reset(self):
        """Reset this symbolic executor.

        Note:
            The symbolic executor timeout, its preconditions and its handlers are NOT reset.
        """
        self.path = Path()
        self.solver = z3.Solver()
        self.model = None

    def _log(self, msg, level=logging.INFO):
        """Log a message with the given level.

        Note:
            The log includes the current path and the caller pid.

        Args:
            msg (str): The message.
            level (int): The level of the log.
        """
        pid = os.getpid()
        path = self.path
        logger.log(level, '[%d][%r] %s', pid, path, msg)

    def ensure(self, bool_ref):
        """Add a boolean expression to the current path condition.

        Args:
            bool_ref (z3.BoolRef): The boolean expression.
        """
        solver_assert(self.solver, [bool_ref])

    def _notify_handlers(self, event, *args, **kwargs):
        """Call all handlers registered for the given event.

        Args:
            *args (:list:object): The positional parameters passed to the handlers.
            **kwargs (:dict:object): The keyword parameters passed to the handlers.
        """
        for handler in self.handlers[event]:
            handler(self, *args, **kwargs)

    def add_handler(self, event, handler):
        """Add a handler for the given event.

        Args:
            event (Event): The event on which the given handler will be called.
            handler (function): The handler.
        """
        self.handlers[event].append(handler)

    def remove_handler(self, event, handler):
        """Remove a handler for the given event.

        Args:
            event (Event): The event of the handler.
            handler (function): The handler.
        """
        self.handlers[event].remove(handler)

    @untraceable
    def _explore(self, bool_ref):
        """Replacement method for the `__bool__` and `__nonzero__` methods of `z3.BoolRef`.

        This method forks the execution of the current process according to the result of `on_branch`
        and enables the symbolic execution of both branches of the current conditional.

        Args:
            bool_ref (z3.BoolRef): The boolean expression.
        """

        # Extract the frame corresponding to the current condition in the function under analysis.
        frame = callerframe(4)
        condition = Condition(frame.function, frame.lineno)

        # Prevent reference cycles.
        del frame

        self._notify_handlers(Event.ON_BEFORE_BRANCH)

        branches = self.on_branch(condition, bool_ref)
        if not branches:
            self._log('explore no branch')
            raise DeadEnd
        elif len(branches) == 1:
            branch, = branches
            self._log(f'explore one branch: {branch}')
        else:
            self._log('explore both branches')
            self._notify_handlers(Event.ON_BEFORE_FORK)
            pid = os.fork()
            if pid:
                os.waitpid(pid, 0)
                branch = branches[1]
            else:
                branch = branches[0]
            self._notify_handlers(Event.ON_AFTER_FORK, branch)

        self.path.append(Decision(condition, branch))
        self._notify_handlers(Event.ON_AFTER_BRANCH)

        # Build the boolean expression corresponding to the target condition branch.
        c = bool_ref
        if not branch:
            c = mk_not(c)

        # Assume the resulting boolean expression.
        self._log(f'assume: {c}')
        self.ensure(c)

        # Add preconditions.
        self.solver.push()
        self.ensure(mk_and(self.preconditions))

        # Solve the resulting path condition.
        self._notify_handlers(Event.ON_BEFORE_CHECK)
        self._log('solve path condition')
        status = self.solver.check()
        self._notify_handlers(Event.ON_AFTER_CHECK, status)

        # Retract preconditions.
        self.solver.pop()

        if status == z3.sat:
            self._log('reachable')
            model = self.solver.model()
            self._log(f'model: {model}')
            self.model = model
        else:
            self._log(f'unreachable: {status}')
            raise DeadEnd
        return branch

    def run(self, f, *args, **kwargs):
        """Run the given function on the given (possibly symbolic) arguments.

        Args:
            f (function): The target callable to run.
            *args (:list:object): The positional parameters passed to `f`.
            **kwargs (:dict:object): The keyword parameters passed to `f`.
        """
        self._log(f'running callable {f.__name__}')

        # Event variable used to notify the main thread that the runner has become the leader of its process group.
        pgroup_set = multiprocessing.Event()

        def runner():
            # Notify the main thread that this runner has become the leader of its process group.
            os.setsid()
            pgroup_set.set()

            with patch_environ(lambda bool_ref: self._explore(bool_ref)):
                try:
                    output = f(*args, **kwargs)
                    self._log(f'output: {output}')
                    self._notify_handlers(Event.ON_RETURN, output)
                except DeadEnd as e:
                    self._log('dead end')
                    self._notify_handlers(Event.ON_DEAD_END)
                except Exception as e:
                    self._log(f'exception: {e}')
                    self._notify_handlers(Event.ON_EXCEPT, e)
                    if PRINT_TRACEBACK:
                        traceback.print_exc()
                finally:
                    self._log('exit')
                    self._notify_handlers(Event.ON_EXIT)
                    os._exit(os.EX_OK)

        # Run `runner` as a separate process.
        p = multiprocessing.Process(target=runner)
        p.start()

        # Wait for `runner` to become the leader of its process group.
        pgroup_set.wait()

        # Wait for at most `timeout` seconds the termination of `runner`.
        p.join(timeout=self.timeout)

        # If `runner` is still alive, kill it and all of its children by referencing its process group.
        if p.is_alive():
            self._notify_handlers(Event.ON_TIMEOUT)

            # Kill `runner` and all of its children.
            try:
                os.killpg(os.getpgid(p.pid), signal.SIGTERM)
            except ProcessLookupError:
                pass

            # Rejoin to prevent spawning zombie processes.
            p.join()

    def __repr__(self):
        """Return a string representing this symbolic executor.

        Returns:
            (str): A string representing this symbolic executor.
        """
        return f'SymbolicExecutor(path={self.path!r}, pc={self.solver!r})'


class RandomWalkSymbolicExecutor(SymbolicExecutor):
    """A symbolic executor that explores both branches of each conditional in random order."""
    def __init__(self, rng):
        """Create a new random-walks ymbolic executor."""
        super().__init__()
        self.rng = rng
        self.random_seeds = [0, 0]

    @override(SymbolicExecutor)
    def on_branch(self, condition, bool_ref):
        """Return randomly either (True, False) or (False, True).

        Returns:
            (:tuple:Bool): Either (True, False) or (False, True).
        """
        return self.rng.sample([True, False], 2)

    @override(SymbolicExecutor)
    def run(self, f, *args, **kwargs):
        # To guarantee that the choices made by spawn subprocesses are random, before branching generate two random
        # seeds from the given random number generator, and use them to reseed the subprocesses after branching.

        def prepare_random_seeds(se):
            se.random_seeds[0] = self.rng.getrandbits(32)
            se.random_seeds[1] = self.rng.getrandbits(32)

        def reseed(se, branch):
            se.rng.seed(self.random_seeds[branch])

        # Bypass `add_handler` to make sure that these handlers are called first.
        self.handlers[Event.ON_BEFORE_FORK].insert(0, prepare_random_seeds)
        self.handlers[Event.ON_AFTER_FORK].insert(0,  reseed)

        super().run(f, *args, **kwargs)

        self.remove_handler(Event.ON_BEFORE_FORK, prepare_random_seeds)
        self.remove_handler(Event.ON_AFTER_FORK, reseed)


class PolicyGuidedSymbolicExecutor(SymbolicExecutor):
    """A symbolic executor guided by a policy."""
    def __init__(self, policy, history_size):
        super().__init__()
        self.policy = policy
        self.history_size = history_size

    @override(SymbolicExecutor)
    def on_branch(self, condition, bool_ref):
        history = tuple(self.path.decisions[-(self.history_size + 1):-1])
        options = self.policy[condition, history]
        if len(options) == 1:
            option, = options
            logger.debug('policy forces deterministic choice: %r', option)
            return option,
        return True, False
