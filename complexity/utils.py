import inspect
import itertools
import operator
import os
import signal
import timeit
import z3

from collections import Counter
from contextlib import contextmanager
from multiprocessing import Process
from multiprocessing import Queue
from multiprocessing import Event
from queue import Empty


def nwise(iterable, n=2):
    """Return an iterator on consecutive n-tuples of elements in the given iterable.

    Args:
        iterable (:iterable:object): The iterable.
        n (int): The number of elements for each tuple.

    Returns:
        (:iterator:tuple:object): An iterator on consecutive n-tuples of elements in the given iterable.
    """
    t = itertools.tee(iterable, n)
    for i in range(1, n):
        for j in range(0, i):
            next(t[i], None)
    return zip(*t)


def pairwise(iterable):
    """Return an iterator on consecutive pairs of elements in the given iterable.

    Args:
        iterable: The iterable.

    Returns:
        An iterator on consecutive pairs of elements in the given iterable.
    """
    return nwise(iterable, n=2)


def callerframe(depth=0):
    """Return the frame of the `depth`-nth caller of the function that invoked this one.

    If no such caller exists, return the top-level frame.

    Args:
        depth (int, optional): The depth of the caller frame to retrieve.

    Returns:
        (inspect.FrameInfo): The frame of the `depth`-nth caller of the function that invoked this one.
    """
    frames = inspect.stack()
    return frames[min(depth, len(frames) - 1)]


patches = dict()
"""
Dictionary storing patches.

Each pair (obj, attr) is mapped to the original value of obj.attr that was patched by calling patch(obj, attr).
"""


def patch(obj, attr, repl):
    """Substitute the attribute `attr` of the object `obj` with `repl`.

    Args:
        obj (object): The object.
        attr (str): The attribute.
        repl (object): The replacement.
    """
    patches[(obj, attr)] = getattr(obj, attr) if hasattr(obj, attr) else None
    setattr(obj, attr, repl)


def unpatch(obj, attr):
    """Restore the original value of the attribute `attr` of the patched object `obj`.

    Note:
        If the attribute was originally missing it is removed instead.

    Args:
        obj (object): The object.
        attr (str): The attribute.
    """
    repl = patches[(obj, attr)]
    if repl:
        setattr(obj, attr, repl)
    else:
        delattr(obj, attr)
    del patches[(obj, attr)]


def get_model(solver):
    """Return the model stored by the given solver if it is available.

    Args:
        solver (z3.Solver): The solver

    Returns:
        (z3.ModelRef): The model stored by the given solver if it is available, `None` if it is not.
    """
    try:
        return solver.model()
    except z3.z3types.Z3Exception:
        return None


def to_python_value(z3_const):
    """Convert a Z3 constant into a python object.

    Args:
        z3_const (z3.ExprRef): The Z3 constant.

    Returns:
        (object): A python object corresponding to the given constant.
    """
    if isinstance(z3_const, z3.IntNumRef):
        return z3_const.as_long()
    elif isinstance(z3_const, z3.BoolRef):
        return z3.is_true(z3_const)
    raise TypeError


def conjuncts(bool_ref):
    """Return the list of conjuncts in a Z3 boolean expression.

    Args:
        bool_ref (z3.BoolRef): The Z3 expression.

    Returns:
        (:list:z3.BoolRef): The list of conjuncts in the given boolean expression.
    """
    if bool_ref.decl().kind() != z3.Z3_OP_AND:
        return [bool_ref]

    result = []
    for child in bool_ref.children():
        result.extend(conjuncts(child))
    return result


def model_to_dict(model):
    """Return a dictionary mapping variable names to python values from the given Z3 model.

    Args:
        model (z3.Model): The Z3 model.

    Returns:
        (:dict:object): A dictionary of python values representing the given model.
    """
    return {key.name(): to_python_value(model[key]) for key in model}


def clean_benchmark_string(benchmark_string):
    """TODO: comment and test."""

    # Drop the irrelevant preamble and suffix of the representation produced by Z3.
    # Then remove useless newline characters.
    benchmark_string = '\n'.join(benchmark_string.split('\n')[2:-2])
    return benchmark_string.replace('\n', '')


def solver_to_pc(solver):
    """Return a string in SMT-LIB v2 format describing the current path condition held by solver.

    Args:
        solver (z3.Solver): The solver.

    Returns:
        (str): A string in SMT-LIB v2 format describing the current path condition held by solver.
    """
    return clean_benchmark_string(solver.to_smt2())


def to_smt2(bool_ref):
    """TODO: comment and test."""
    v = (z3.Ast * 0)()
    benchmark_string = z3.Z3_benchmark_to_smtlib_string(
        bool_ref.ctx_ref(),
        "benchmark",  # benchmark name
        "",  # logic
        "unknown",  # benchmark category
        "",
        0,
        v,
        bool_ref.as_ast()
    )
    return clean_benchmark_string(benchmark_string)


def negate(bool_ref):
    """Return a simplified expression representing the negation of the given expression.

    Args:
        bool_ref (z3.BoolRef): The boolean expression.

    Returns:
        (z3.BoolRef): A simplified expression representing the negation of the given expression.
    """
    return z3.simplify(z3.Not(bool_ref))


def pp_model_dict(model):
    """Print a dictionary representing a model to stdout in human-readable format.

    The entries are sorted lexicographically by key, with shorter strings preceding before longer strings.

    Args:
        model (:dict:object): The model.
    """
    items = list(model.items())
    for key, value in sorted(items, key=lambda p: (len(p[0]), p[0])):
        print(key, '=', value)


def unravel(gen, timeout=60):
    """Extract as many elements as possible from the given generator within the given timeout.

    Args:
        gen (:generator:object): The generator.
        timeout (float, optional): The maximum number of seconds to unravel the given generator.

    Returns:
        (:list:object): The list of the elements extracted.
    """
    # Flag to notify the main process that the spawn process has become the leader of its process group.
    pgroup_set = Event()

    def unraveler(gen, queue):
        os.setsid()
        pgroup_set.set()

        for e in gen:
            queue.put(e)

    queue = Queue()
    p = Process(target=unraveler, args=(gen, queue))
    p.start()

    # Wait for the spawn process to become the leader of its process group.
    pgroup_set.wait()

    p.join(timeout=timeout)

    result = []
    while True:
        try:
            result.append(queue.get_nowait())
        except Empty:
            break

    if p.is_alive():
        # Kill the spawn process and all of its subprocesses.
        try:
            os.killpg(os.getpgid(p.pid), signal.SIGTERM)
        except ProcessLookupError:
            pass
        p.join()

    return result


def most_common(iterable):
    """Return the most common element in the given iterable.

    Args:
        iterable (:iterable:object): The iterable.

    Returns:
        (object): The most common element in the given iterable.
    """
    return Counter(iterable).most_common(1)[0][0]


def pick_prob(l, e):
    """Return the probability of randomly choosing the element `e` from the list `l`.

    Args:
        l (:list:object): The list.
        e (object): The element.

    Returns:
        (float): The probability of randomly choosing the element `e` from the list `l`.
    """
    return l.count(e) / len(l)


def assert_in_range(expr_ref, start, end):
    """Return a Z3 expression stating that `start` <= `expr_ref` <= `end`.

    Args:
        expr_ref (z3.ExprRef): The arithmetic expression.
        start (int): The lower bound.
        end (int): The upper bound.

    Returns:
        (z3.BoolRef): A Z3 expression stating that `start` <= `expr_ref` <= `end`.
    """
    return z3.And(expr_ref >= start, expr_ref <= end)


def assert_sorted(iterable, ascending=True, strict=False):
    """Return a Z3 expression stating that the given iterable is sorted.

    Args:
        iterable (:iterable:z3.ExprRef): The iterable.
        ascending (bool): `True` for ascending order, `False` for descending.
        strict (bool): `True` for strict comparison, `False` for relaxed comparison.

    Returns:
        (z3.BoolRef): A Z3 expression stating that the given iterable is sorted.
    """
    op = {
        True: {
            True: operator.lt,
            False: operator.le,
        },
        False: {
            True: operator.gt,
            False: operator.ge,
        }
    }[ascending][strict]
    return z3.And([op(e1, e2) for e1, e2 in pairwise(iterable)])


def assert_all_different(iterable):
    """Return a Z3 expression stating that all elements in the given iterable are different.

    Args:
        iterable (:iterable:z3.ExprRef): The iterable.

    Returns:
        (z3.BoolRef): A Z3 expression stating that all elements in the given iterable are different.
    """
    return z3.And([e1 != e2 for e1, e2 in itertools.combinations(iterable, 2)])


def assert_all_positive(iterable):
    """Return a Z3 expression stating that all elements in the given iterable are positive.

    Args:
        iterable (:iterable:z3.ExprRef): The iterable.

    Returns:
        (z3.BoolRef): A Z3 expression stating that all elements in the given iterable are positive.
    """
    return z3.And([e > 0 for e in iterable])


def override(cls):
    """Decorator ensuring that a class method is a proper override.

    Args:
        cls (class): The class declaring the overridden method.

    Returns:
        The decorated method.
    """
    def inner(method):
        assert(method.__name__ in dir(cls))
        return method
    return inner


def pp_wcet_profiles(profiles):
    """Print the input profiles.

    Args:
        profiles (:iterable:PathProfile): The profiles.
    """
    for i, profile in enumerate(profiles):
        print(f'id: {i}')
        print(f'cost: {profile.cost!r}')
        print(f'complete: {profile.complete!r}')
        print(f'path: {profile.path!r}')
        print(f'pc: {profile.pc!r}')
        pp_model_dict(profile.model)
        print()


@contextmanager
def stopwatch(msg):
    """Print the time spent in this context at its end.

    Args:
        msg (str): A string prefix to the print message.
    """
    tic = timeit.default_timer()
    try:
        yield
    finally:
        tac = timeit.default_timer()
        print('%s: %.2f seconds' % (msg, tac - tic))


def _to_ast_array(args):
    sz = len(args)
    _args = (z3.Ast * sz)()
    for i in range(sz):
        _args[i] = args[i].as_ast()
    return _args, sz


def mk_not(bool_ref):
    """Return the negation of the given constraint.

    This method bypasses most type checks enforced by the Z3 Python bindings.

    Args:
        bool_ref (z3.BoolRef): The constraint to negate.

    Returns:
        (z3.BoolRef): The negation of the given constraint.
    """
    ctx = z3.main_ctx()
    return z3.BoolRef(z3.Z3_mk_not(ctx.ref(), bool_ref.as_ast()), ctx)


def mk_and(bool_refs):
    """Return the conjunction of the given constraints.

    This method bypasses most type checks enforced by the Z3 Python bindings.

    Args:
        bool_refs (:list:z3.BoolRef): The list of constraints.

    Returns:
        (z3.BoolRef): The conjunction of the given constraints.
    """
    ctx = z3.main_ctx()
    _args, sz = _to_ast_array(bool_refs)
    return z3.BoolRef(z3.Z3_mk_and(ctx.ref(), sz, _args), ctx)


def mk_implies(bool_ref1, bool_ref2):
    """Return the implification of the given constraints.

    This method bypasses most type checks enforced by the Z3 Python bindings.

    Args:
        bool_ref1 (z3.BoolRef): The implying constraint.
        bool_ref2 (z3.BoolRef): The implied constraint.

    Returns:
        (z3.BoolRef): The implification of the given constraints.
    """
    ctx = z3.main_ctx()
    return z3.BoolRef(
        z3.Z3_mk_implies(
            ctx.ref(),
            bool_ref1.as_ast(),
            bool_ref2.as_ast()),
        ctx
    )


def solver_assert(solver, bool_refs):
    """Assert the given constraints in the given solver.

    This method bypasses most type checks enforced by the Z3 Python bindings.

    Args:
        solver (z3.Solver): The solver.
        bool_refs (:list:z3.BoolRef): The list of constraints to assert.
    """
    ctx = z3.main_ctx()
    for bool_ref in bool_refs:
        z3.Z3_solver_assert(ctx.ref(), solver.solver, bool_ref.as_ast())


solver = z3.Solver()
"""Global solver to back Z3 related utility functions."""


def quick_check(bool_refs):
    """Return the satisfiability of the conjunction of the given constraints.

    This method bypasses most type checks enforced by the Z3 Python bindings and is thus generally
    faster then instantiating a solver, asserting the given constraints and checking them.

    Args:
        bool_refs (:list:z3.BoolRef): The list of constraints to solve.

    Returns:
        (bool): The satisfiability of the conjunction of the given constraints.
    """
    solver.push()
    solver_assert(solver, bool_refs)
    status = solver.check()
    solver.pop()
    return status


def is_contradiction(c):
    """Return `True` if `c` is a contradiction.

    Args:
        c (z3.BoolRef): The constraint.

    Returns:
        (bool): `True` if `c` is a contradiction.
    """
    return quick_check([c]) == z3.unsat


def is_tautology(c):
    """Return `True` if `c` is a tautology.

    Args:
        c (z3.BoolRef): The constraint.

    Returns:
        (bool): `True` if `c` is a tautology.
    """
    return quick_check([mk_not(c)]) == z3.unsat


def is_implied(c, constraint_set):
    """Return `True` if `c` is implied by the conjunction of the constraints in `constraint_set`

    Args:
        c (z3.BoolRef): The constraint.
        constraint_set (:list:z3.BoolRef): The constraints that might imply `c`.

    Returns:
        (bool): `True` if `c` is implied by the conjunction of the constraints in `constraint_set`.
    """
    return is_tautology(mk_implies(mk_and(constraint_set), c))


def is_inconsistent(constraint_set):
    """Return `True` if the given constraint set is inconsistent.

    Args:
        constraint_set (:list:z3.BoolRef): The constraint set.

    Returns:
        (bool): `True` if the given constraint set is inconsistent.
    """
    return is_contradiction(mk_and(constraint_set))


def minimize(constraint_set):
    """Remove the constraints that are implied by the remaining constraints in the given set.

    Args:
        constraint_set (:list:z3.BoolRef): The list of constraints.

    Returns:
        (:list:z3.BoolRef): A copy of the input list with no implied constraints.
    """
    i = 0
    constraint_set = list(constraint_set)
    while i < len(constraint_set):
        if is_implied(constraint_set[i], constraint_set[:i] + constraint_set[i+1:]):
            constraint_set.pop(i)
        else:
            i += 1
    return constraint_set
