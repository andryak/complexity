import z3


def slist(name, size):
    """Return a new list of symbolic integers of the given size.

    Args:
        name (str): The name of the list.
        size (int): The number of symbolic elements in the list.

    Returns:
        (:list:z3.ArithRef): A list of symbolic integers of the given size.
    """
    return [z3.Int(f'{name}[{i}]') for i in range(size)]


class Graph(object):
    """A graph implemented with an adjacency matrix."""
    def __init__(self, matrix):
        self.matrix = matrix

    def has_edge(self, a, b):
        return self.matrix[a][b]

    def add_edge(self, a, b):
        self.matrix[a][b] = True

    def remove_edge(self, a, b):
        self.matrix[a][b] = False

    def nodes(self):
        return list(range(len(self.matrix)))

    def edges(self):
        r = []
        for i, row in enumerate(self.matrix):
            for j, e in enumerate(row):
                if e:
                    r.append((i, j))
        return r

    def neighbours(self, n):
        r = []
        for i, e in enumerate(self.matrix[n]):
            if e:
                r.append(i)
        return r

    def __str__(self):
        return str(self.matrix)

    def __repr__(self):
        return self.__str__()


def sgraph(name, size):
    """Return a new symbolic graph of the given size.

    Args:
        name (str): The name of the graph.
        size (int): The number of nodes in the graph.

    Returns:
        (Graph): A symbolic graph of the given size.
    """
    matrix = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(z3.Bool(f'{name}[{i}][{j}]'))
        matrix.append(row)
    return Graph(matrix)


class WGraph(object):
    """A weighted graph implemented with an adjacency matrix."""
    def __init__(self, matrix):
        self.matrix = matrix

    def has_edge(self, a, b):
        return self.matrix[a][b] is not None

    def add_edge(self, a, b, weight):
        self.matrix[a][b] = weight

    def remove_edge(self, a, b):
        self.matrix[a][b] = None

    def nodes(self):
        return list(range(len(self.matrix)))

    def edges(self):
        r = []
        for i, row in enumerate(self.matrix):
            for j, w in enumerate(row):
                if w is not None:
                    r.append((i, j, w))
        return r

    def neighbours(self, n):
        r = []
        for i, w in enumerate(self.matrix[n]):
            if w is not None:
                r.append((i, w))
        return r

    def __str__(self):
        return str(self.matrix)

    def __repr__(self):
        return self.__str__()


def swgraph(name, size):
    """Return a new symbolic weighted graph of the given size.

    Args:
        name (str): The name of the graph.
        size (int): The number of nodes in the graph.

    Returns:
        (WGraph): A symbolic weighted graph of the given size.
    """
    matrix = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(z3.Int(f'{name}[{i}][{j}]'))
        matrix.append(row)
    return WGraph(matrix)
