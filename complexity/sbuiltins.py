import z3


class srange(object):
    """Range-like iterable that also supports symbolic expressions."""
    def __init__(self, *args, **kwargs):
        if kwargs:
            raise TypeError("range() does not take keyword arguments")
        elif not args:
            raise TypeError("range expected 1 arguments, got 0")
        elif len(args) == 3 and args[2] == 0:
            raise ValueError("range() arg 3 must not be zero")
        elif len(args) > 3:
            raise TypeError(f"range expected at most 3 arguments, got {len(args)}")

        start = 0
        step = 1

        if len(args) == 1:
            stop = args[0]
        elif len(args) == 2:
            start = args[0]
            stop = args[1]
        else:
            start = args[0]
            stop = args[1]
            step = args[2]

        def check(v):
            if not isinstance(v, z3.ArithRef) and not isinstance(v, int):
                tname = type(stop).__name__
                raise TypeError(f"'{tname}' object cannot be interpreted as an integer")

        check(start)
        check(stop)
        check(step)

        self.start = start
        self.stop = stop
        self.step = step

    def __len__(self):
        if self.step > 0 and self.start >= self.stop:
            return 0
        elif self.step < 0 and self.start <= self.stop:
            return 0

        return sabs(self.stop - self.start) // sabs(self.step)

    def __iter__(self):
        if self.step > 0 and self.start >= self.stop:
            return
        elif self.step < 0 and self.start <= self.stop:
            return

        i = self.start
        while i != self.stop:
            yield(i)
            i += self.step


def sabs(v):
    """Return the absolute value of `v` and also supports symbolic expressions.

    Args:
        v (object): The original value.

    Returns:
        (object): The absolute value of `v`.
    """
    if v < 0:
        return -v
    return v
