import logging

from collections import defaultdict
from collections import namedtuple
from complexity.symex import Event
from complexity.symex import PolicyGuidedSymbolicExecutor
from complexity.symex import SymbolicExecutor
from complexity.tracer import FunScopeTracer
from complexity.tracer import traced
from complexity.utils import model_to_dict
from complexity.utils import nwise
from complexity.utils import solver_to_pc
from complexity.utils import unravel
from multiprocessing import Event as MPEvent
from multiprocessing import Manager


logger = logging.getLogger(__name__)


PathProfile = namedtuple('PathProfile', [
    'path',
    'cost',
    'pc',
    'model',
    'complete'
])
"""A path profile describes a program's path and its associated cost."""


def path_profiles(se, f, args, kwargs):
    """Return the list of all path profiles of the input function.

    Args:
        se (SymbolicExecutor): The symbolic executor to produce the profiles.
        f (function): The target function.
        args (:tuple:object): The positional parameters for `f`.
        kwargs (:dict:object): The keyword parameters for `f`.

    Returns:
        (:list:PathProfile): All path profiles of the given function.
    """
    logger.debug('collecting path profiles for callable %s', f.__name__)

    # Instrument the target function so that it also stores the number of instructions it executes in `tracer`.
    tracer = FunScopeTracer()
    f = traced(tracer)(f)

    # Create a new manager holding a shared list to store the profiles collected during symbolic execution.
    manager = Manager()
    profiles = manager.list()

    # Create an event to lock the list on timeout, the event is set by default and unset on timeout.
    can_write_profiles = MPEvent()
    can_write_profiles.set()

    def register_profile(tracer, se, complete):
        """Register a profile in the profiles list atomically."""
        nr_instr = tracer.nr_instr
        path = se.path
        pc = solver_to_pc(se.solver)
        model = model_to_dict(se.model or {})

        profile = PathProfile(path, nr_instr, pc, model, complete)
        logger.debug('collecting profile: %r', profile)

        can_write_profiles.wait()
        profiles.append(profile)

    def on_timeout(se):
        # Prevent processes to write to the profiles list.
        can_write_profiles.clear()

    def on_before_branch(se):
        register_profile(tracer, se, False)

    def on_return(se, output):
        register_profile(tracer, se, True)

    def on_except(se, e):
        register_profile(tracer, se, True)

    # Register event handlers to store profiles.
    se.add_handler(Event.ON_TIMEOUT, on_timeout)
    se.add_handler(Event.ON_BEFORE_BRANCH, on_before_branch)
    se.add_handler(Event.ON_RETURN, on_return)
    se.add_handler(Event.ON_EXCEPT, on_except)

    # Run `f` symbolically and store profiles.
    se.run(f, *args, **kwargs)

    # Unregister all event handlers to store profiles.
    se.remove_handler(Event.ON_TIMEOUT, on_timeout)
    se.remove_handler(Event.ON_BEFORE_BRANCH, on_before_branch)
    se.remove_handler(Event.ON_RETURN, on_return)
    se.remove_handler(Event.ON_EXCEPT, on_except)

    logger.debug('collected path profiles:')
    for profile in profiles:
        logger.debug(profile)

    return list(profiles)


def wcetpp(se, f, args, kwargs):
    """Return all path profiles of the function `f` of maximal cost.

    Args:
        se (SymbolicExecutor): The symbolic executor to produce the profiles.
        f (function): The target function.
        args (:tuple:object): The positional parameters for `f`.
        kwargs (:dict:object): The keyword parameters for `f`.

    Returns:
        (:list:PathProfile): All path profiles of the function `f` of maximal cost.
    """
    logger.debug('collecting wcet path profiles for callable %s', f.__name__)

    profiles = path_profiles(se, f, args, kwargs)
    if not profiles:
        return profiles

    max_cost = max(profiles, key=lambda e: e.cost).cost
    logger.debug('maximal profile cost: %d', max_cost)

    wcet_profiles = [profile for profile in profiles if profile.cost == max_cost]

    logger.debug('collected wcet path profiles:')
    for profile in wcet_profiles:
        logger.debug(profile)

    return wcet_profiles


def inc_wcetpp(f, args_gen, kwargs_gen, preconditions_gen, min_size, max_size, timeout):
    """Return as many exact wcet path profiles as possible for the function `f` within the given timeout.

    This function runs `wcetpp` on the target callable with parameters of size ranging from `min_size` to `max_size`
    generated with `args_gen` and `kwargs_gen` and constraints generated with `preconditions_gen`. After all profiles
    have been collected or the timeout expires this function returns a list of tuples of the form (size, profiles).

    Args:
        f (function): The target function.
        args_gen (int -> :tuple:object): The positional parameters generator for `f`.
        kwargs_gen (int -> :dict:object): The keyword parameters generator for `f`.
        preconditions_gen (int -> :list:z3.BoolRef): The preconditions generator.
        min_size (int): The minimum input size.
        max_size (int): The maximum input size.
        timeout (float): The timeout in seconds.

    Returns:
        (:list:(:tuple:(int, PathProfile))): A list of pairs (size, profiles).
    """
    def wcetpp_gen():
        for size in range(min_size, max_size+1):
            logger.debug(f'collecting wcet path profiles of size {size}')

            args = args_gen(size)
            kwargs = kwargs_gen(size)

            se = SymbolicExecutor()
            se.preconditions = preconditions_gen(size, args, kwargs)
            profiles = wcetpp(se, f, args, kwargs)
            yield size, profiles
    return unravel(wcetpp_gen(), timeout)


def to_policy(path, history_size):
    """Return a policy from the given path.

    Args:
        path (Path): The path.
        history_size (int): The number of past choices to consider in the policy.

    Returns:
        (:defaultdict:(:tuple:object, :set:bool)): The policy.
    """
    policy = defaultdict(set)

    for decisions in nwise(path.decisions, n=history_size+1):
        history, last = decisions[:-1], decisions[-1]
        policy[last.condition, history].add(last.chosen_branch)

    return policy


def rank_policy(policy):
    """Return the number of deterministic choices enforced by the given policy.

    Args:
        policy (:defaultdict:(:tuple:object, :set:bool)): The policy.

    Returns:
        (int): The number of deterministic choices enforced by the given policy.
    """
    rank = 0
    for condition, options in policy.items():
        if len(options) == 1:
            rank += 1
    return rank


def best_policy(paths, history_size):
    """Return the policy of maximal rank amongst the ones derived by the given paths.

    Args:
        paths (:list:Path): A list of paths.
        history_size (int): The history size of the policies generated from the given paths.

    Returns:
        (:defaultdict:(:tuple:object, :set:bool)): The maximal rank policy amongst the ones derived by the given paths.
    """
    policies = (to_policy(path, history_size) for path in paths)
    return max(
        policies,
        key=lambda policy: rank_policy(policy),
        default=defaultdict(set)
    )


def is_deterministic(policy):
    """Return `True` if policy only enforces deterministic choices.

    Args:
        policy (:defaultdict:(:tuple:object, :set:bool)): The policy.

    Returns:
        (bool): `True` if policy only enforces deterministic choices.
    """
    return all(len(options) == 1 for condition, options in policy.items())


def analyze_wcet(
        f,
        args_gen,
        kwargs_gen,
        preconditions_gen,
        min_training_size,
        max_training_size,
        timeout,
        target_size,
        history_size):

    """Return the worst case execution paths of the given callable on input of size `target`."""

    assert(min_training_size <= max_training_size < target_size)
    logger.info('analyzing wcet of callable %s', f.__name__)

    # Phase 1: build a unified policy within the given timeout.

    profile_list = inc_wcetpp(
        f,
        args_gen,
        kwargs_gen,
        preconditions_gen,
        min_training_size,
        max_training_size,
        timeout
    )

    logger.info('wcet path profiles list:')
    for size, profiles in profile_list:
        logger.info('wcet path profiles for size %d:', size)
        for profile in profiles:
            logger.info(profile)

    logger.info('building unified policy')
    unified_policy = defaultdict(set)
    for size, profiles in profile_list:
        policy = best_policy((profile.path for profile in profiles), history_size)
        unified_policy.update(policy)

    logger.info('unified policy:')
    for condition, options in unified_policy.items():
        logger.info('%r = %r', condition, options)

    # Phase 2: guide symbolic execution on larger inputs using the unified policy.

    args = args_gen(target_size)
    kwargs = kwargs_gen(target_size)
    preconditions = preconditions_gen(target_size, args, kwargs)

    gse = PolicyGuidedSymbolicExecutor(unified_policy, history_size)
    gse.timeout = timeout
    gse.preconditions = preconditions
    return wcetpp(gse, f, args, kwargs)


def main():
    import argparse
    import importlib

    from complexity.utils import pp_wcet_profiles

    parser = argparse.ArgumentParser(description='WCET input generator.')
    parser.add_argument('module', help='path to a python module')
    parser.add_argument('function', help='name of a function within the module')
    parser.add_argument('size', type=int, help='target size of the WCET input to generate')
    parser.add_argument('--args-gen', default='args_gen', help='name of a function to generate positional arguments')
    parser.add_argument('--kwargs-gen', default='kwargs_gen', help='name of a function to generate keyword arguments')
    parser.add_argument('--pre-gen', default='preconditions_gen', help='name of a function to generate preconditions')
    parser.add_argument('--timeout', type=float, default=60.0, help='timeout for the generation, in seconds')
    pargs = parser.parse_args()

    module_ = importlib.import_module(pargs.module)

    args = getattr(module_, pargs.args_gen)(pargs.size)
    kwargs = getattr(module_, pargs.kwargs_gen)(pargs.size)

    se = SymbolicExecutor()
    se.timeout = pargs.timeout
    se.preconditions = getattr(module_, pargs.pre_gen)(pargs.size, args, kwargs)

    profiles = wcetpp(
        se,
        getattr(module_, pargs.function),
        args,
        kwargs,
    )
    pp_wcet_profiles(profiles)


if __name__ == '__main__':
    main()
