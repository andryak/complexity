from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def algox(l):
    """Return the sum of the elements in the subsequence of A that produces the largest sum.

    Expected worst case: unknown.
    """
    y = -(1 << 32)
    i = 0
    j = 0
    x = 0
    while i < len(l):
        x = x + l[j]
        if x > y:
            y = x
        if j == len(l) - 1:
            i = i+1
            j = i
            x = 0
        else:
            j = j+1
    return y


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 4
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algox,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=3,
            timeout=5,
            target_size=4,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
