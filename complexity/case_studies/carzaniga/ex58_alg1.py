from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def alg1(l):
    """Bubblesort.

    Note:
        Expected worst case: list sorted in reverse order.
    """
    for i in range(len(l) - 1, 0, -1):
        s = True
        for j in range(1, i+1):
            if l[j-1] > l[j]:
                l[j-1], l[j] = l[j], l[j-1]
                s = False
        if s:
            return


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            alg1,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Policy is deterministic.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            alg1,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
