from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def alg2(l):
    """Moves the maximum value to the end of the array.

    Note:
        Expected worst case: the maximum is the first element of the input list.
    """
    i = 0
    j = len(l) - 1
    while i < j:
        if l[i] > l[j]:
            l[i], l[i+1] = l[i+1], l[i]
            if i + 1 < j:
                l[i], l[j] = l[j], l[i]
            i += 1
        else:
            j -= 1


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            alg2,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Policy is deterministic.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            alg2,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
