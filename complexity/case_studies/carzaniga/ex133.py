from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def algox(l):
    """Return the number of occurrences of the most frequent element in A.

    Expected worst case: unknown.
    """
    B = l
    i = 0
    while i < len(B):
        j = i+1
        while j < len(B):
            if B[j] == B[i]:
                i = i+1
                B[i], B[j] = B[j], B[i]
            j = j+1
        i = i+1

    q = B[0]
    n = 1
    m = 1
    for i in range(1, len(B)):
        if B[i] == q:
            n = n+1
            if n > m:
                m = m+1
        else:
            q = B[i]
            n = 1
    return m


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algox,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
