from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    return slist('l', size),


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def algox(l):
    """Modify the list A in place.

    All the elements in even positions are sorted in ascending order while the elements in odd positions are
    sorted in descending order and are always greater than or equal to all elements in the even positions.

    Note:
        Expected worst case: unknown.
    """
    i = 0
    while i < len(l) - 1:
        # Calculate the index `p` of the minimum element in A[i:] and
        # the index `q` of the maximum element in A[i:] and
        if l[i] > l[i+1]:
            l[i], l[i+1] = l[i+1], l[i]
        p = i
        q = i+1
        for j in range(i+2, len(l)):
            if l[j] < l[p]:
                p = j
            elif l[j] > l[q]:
                q = j

        # Place the minimum in position `i` and the maximum in position `i+1`.
        # The consider the next two elements.
        l[i], l[p] = l[p], l[i]
        l[i+1], l[q] = l[q], l[i+1]
        i += 2


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    # Non-deterministic policy.
    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algox,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=5,
            timeout=10,
            target_size=6,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
