import z3

from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    return slist('l', size), z3.Int('k')


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def algox(l, k):
    """Delete all occurrences of element k in the array A.

    All elements past the deleted elements to the left (leaving no holes) and writing None in the trailing
    positions of the array left open (corresponding to the deleted elements).

    Note:
        Expected worst case: l = [k, k, k, ...].
    """
    i = 0
    while i < len(l):
        if l[i] == k:
            algoy(l, i)
        else:
            i += 1


def algoy(l, i):
    while i < len(l) - 1:
        l[i] = l[i+1]
        i += 1
    l[i] = None


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            algox,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            algox,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=10,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
