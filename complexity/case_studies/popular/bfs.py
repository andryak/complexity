from complexity.structs import sgraph
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    graph = sgraph('g', size)

    # Remove self-loops.
    for n in graph.nodes():
        graph.remove_edge(n, n)

    return graph, 0, size - 1


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def bfs(g, a, b):
    q = [a]
    visited = set([a])

    while q:
        c = q.pop(0)

        if c == b:
            return True

        for n in g.neighbours(c):
            if n not in visited:
                visited.add(n)
                q.append(n)
    return False


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 4
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)
        se.timeout = 10

        profiles = wcetpp(
            se,
            bfs,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            bfs,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=1,
            max_training_size=5,
            timeout=5,
            target_size=20,
            history_size=1,
        )
    pp_wcet_profiles(profiles)
