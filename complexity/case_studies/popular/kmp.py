from complexity.structs import slist
from complexity.symex import SymbolicExecutor
from complexity.utils import pp_wcet_profiles
from complexity.utils import stopwatch
from complexity.worst_case import analyze_wcet
from complexity.worst_case import wcetpp


def args_gen(size):
    return slist('l', size), slist('p', 3)


def kwargs_gen(size):
    return {}


def preconditions_gen(size, args, kwargs):
    return []


def make_table(pattern):
    ret = [0]
    for i in range(1, len(pattern)):
        j = ret[i - 1]
        while j > 0 and pattern[j] != pattern[i]:
            j = ret[j - 1]
        ret.append(j + 1 if pattern[j] == pattern[i] else j)
    return ret


def kmp(text, pattern):
    """
    Determine the starting indexes of the occurrences of `pattern` in `text` using the KMP algorithm.

    Note:
        Expected worst case: the text does not contain the pattern and,
          1. the text has the form: A...AX,
          2. the pattern has the form A...AY.
    """
    table, ret, j = make_table(pattern), [], 0
    for i in range(len(text)):
        while j > 0 and text[i] != pattern[j]:
            j = table[j - 1]

        if text[i] == pattern[j]:
            j += 1

        if j == len(pattern):
            ret.append(i - j + 1)
            j = 0
    return ret


if __name__ == '__main__':
    with stopwatch('wcetpp'):
        size = 5
        args = args_gen(size)
        kwargs = kwargs_gen(size)

        se = SymbolicExecutor()
        se.preconditions = preconditions_gen(size, args, kwargs)

        profiles = wcetpp(
            se,
            kmp,
            args,
            kwargs,
        )
    pp_wcet_profiles(profiles)

    with stopwatch('analyze_wcet'):
        profiles = analyze_wcet(
            kmp,
            args_gen,
            kwargs_gen,
            preconditions_gen,
            min_training_size=0,
            max_training_size=9,
            timeout=5,
            target_size=10,
            history_size=2,
        )
    pp_wcet_profiles(profiles)
