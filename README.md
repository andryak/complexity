# Complexity

---

[![Codeship Status for andryak/complexity](https://app.codeship.com/projects/c74a4580-b59e-0135-994f-2a09f481580f/status?branch=master)](https://app.codeship.com/projects/258436)

## Abstract

Complexity is a library to determine the worst-case inputs for python callables.
This library exploits symbolic execution and dynamic tracing to generate valid
inputs for python algorithms that produce the longest computation.

Currently, complexity supports algorithms that work with:

+ integers, 
+ lists, and
+ graphs.

## Installation

The application is shipped as a Python 3.6 package.
It depends on the `virtualenv` and `python3.6` binaries which you might need to install, if needed.
To install the application and its dependencies clone the repository, navigate to its main folder
and run the following commands:

```Bash
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
pip install .
```

This creates two executable scripts: `wcet` and `genetic-wcet`; please run the following commands for more information:

```Bash
wcet --help
genetic-wcet --help
```

## Dependencies

Complexity depends on the [Microsoft Z3 SMT solver](https://github.com/Z3Prover/z3)
to determine the satisfiability or the unsatisfiability of formulas generated during
the symbolic execution of the subject callables.

## Logo

Complexity's logo has been designed by [Madebyoliver](http://www.flaticon.com/authors/madebyoliver) 
at [flaticon](http://www.flaticon.com).
