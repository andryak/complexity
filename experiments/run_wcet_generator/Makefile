SOURCEDIR := ../../complexity/case_studies

vpath %.py $(shell find $(SOURCEDIR) -type d)


# Extract target names from this Makefile.
TARGETS := $(shell grep -o "\w\+.txt" Makefile | sort | uniq)

.PHONY: all
all: $(TARGETS)


.PHONY: clean
clean:
	rm -f *.txt


alternate.txt: alternate.py
	(genetic-wcet\
	    complexity.case_studies.others.alternate alternate 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

bfs.txt: bfs.py
	(genetic-wcet\
	    complexity.case_studies.popular.bfs bfs 7\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

binsearch.txt: binsearch.py
	(genetic-wcet\
	    complexity.case_studies.popular.binsearch binsearch 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

bubblesort.txt: bubblesort.py
	(genetic-wcet\
	    complexity.case_studies.popular.bubblesort bubblesort 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

count_palindromes.txt: count_palindromes.py
	(genetic-wcet\
	    complexity.case_studies.others.count_palindromes count_palindromes 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

dfs.txt: dfs.py
	(genetic-wcet\
	    complexity.case_studies.popular.dfs dfs 7\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

ex58_alg2.txt: ex58_alg2.py
	(genetic-wcet\
	    complexity.case_studies.carzaniga.ex58_alg2 alg2 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

ex119.txt: ex119.py
	(genetic-wcet\
	    complexity.case_studies.carzaniga.ex119 algox 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

ex175.txt: ex175.py
	(genetic-wcet\
	    complexity.case_studies.carzaniga.ex175 algox 10\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

is_fib.txt: is_fib.py
	(genetic-wcet\
	    complexity.case_studies.popular.is_fib is_fib 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

is_palindrome.txt: is_palindrome.py
	(genetic-wcet\
	    complexity.case_studies.popular.is_palindrome is_palindrome 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1

kmp.txt: kmp.py
	(genetic-wcet\
	    complexity.case_studies.popular.kmp kmp 50\
	    --generations=300\
	    --population-size=200\
	    --timeout=5\
	    --seed=0\
	) > $@ 2>&1
